import React, { Component } from 'react';
import { Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="header">
      <div > 
         <h1>SAMPLE ROUTING</h1>
      </div>
        <Navbar className="Navbar-Header">           
                
                
          
          <Nav  >
                    <a href="#">Sample</a>
                   <Link  id="navbar-item" to="/home">Home</Link> 
                   <Link   id="navbar-item" to="/about">About</Link> 
                    <Link   id="navbar-item" to="/contact">Contact</Link>
          </Nav>
          
       </Navbar>
      </div>
    );
  }
}

export default App;
